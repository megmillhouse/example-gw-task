# Sample Gravitational-Wave Tasks

First, Read [this science summary](https://www.ligo.org/science/Publication-DataAnalysisGuide/flyer.pdf) of "A Guide to LIGO-Virgo Detector Noise and Extraction of Transient Gravitational-Wave Signals" from the LIGO-Virgo-KAGRA collaboration.
In summary, we can say that the data produced by the detectors is equal to signal plus noise. The signal is the gravitational-wave, and the noise comes from the instrument. 


This directory contains four files. For each of the two LIGO detectors (Hanford and Livingston) there is a data file which includes the detector data, and a signal file which contains one possible GW signal that could have produced the data. In this mini-exercise you will look at the data and the signal, and then test whether the residuals from that data and signal are consistent with the expected noise. The tasks are as follows:

- [ ] Make one plot for each detector that shows the data and the signal.
- [ ] Make and then plot the *residuals* for each detector. This is the signal subtracted from the data.
- [ ] According to the science summary you just read, the "noise should be distributed as ... a Gaussian or Normal distribution". Specifically, we assume it's white noise, or a Normal distribution with a mean of 0 and a variance of 1. Are your residuals the previous step consistent with this distribution? One way to check this is to plot a Normal distribution on top of a histogram of your residuals (hint: if you're using matplotlib or numpy to make your histograms, you may want to inlcude `density=1` ).
